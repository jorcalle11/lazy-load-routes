import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import asyncComponent from './AsyncComponent';

import logo from './logo.svg';
import './App.css';

const Home = asyncComponent(() =>
  import('./Home').then(module => module.default)
);

const Users = asyncComponent(() =>
  import('./Users').then(module => module.default)
);

const Posts = asyncComponent(() =>
  import('./Posts').then(module => module.default)
);

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Lazy load routes</h1>
        </header>
        <Router>
          <div>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/users">Users</Link>
              </li>
              <li>
                <Link to="/posts">Posts</Link>
              </li>
            </ul>
            <Route exact path="/" component={Home} />
            <Route path="/users" component={props => <Users {...props} />} />
            <Route path="/posts" component={Posts} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
