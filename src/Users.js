import React, { Component } from 'react';

class Users extends Component {
  state = {
    users: [
      {
        id: 1,
        name: 'Beverley Douglas',
        email: 'beverley.douglas82@example.com'
      },
      {
        id: 2,
        name: 'Savannah Frazier',
        email: 'savannah.frazier23@example.com'
      },
      { id: 3, name: 'Rodney Lowe', email: 'rodney.lowe78@example.com' },
      { id: 4, name: 'Avery Andrews', email: 'avery.andrews69@example.com' },
      { id: 5, name: 'Kay Day', email: 'kay.day56@example.com' }
    ]
  };

  componentDidMount() {
    const params = this.getParams();
    this.locationChange(params, true);
  }

  dispatchAction = (nextLocation, currentLocation) => {
    console.log('next', nextLocation);
    console.log('current', currentLocation);
  };

  // utility
  getParams = () => {
    const { location } = this.props;
    const params = new URLSearchParams(location.search);

    const [offset, limit, active] = [
      params.has('offset') ? +params.get('offset') : 0,
      params.has('limit') ? +params.get('limit') : 10,
      params.has('active') ? JSON.parse(params.get('active')) : true
    ];

    return { offset, limit, active };
  };

  locationChange = (
    { offset = 0, limit = 10, active = true } = {},
    init = false
  ) => {
    const { history, location } = this.props;

    history.push({
      pathname: '/users',
      search: `?offset=${offset}&limit=${limit}&active=${active}`
    });

    if (!init) {
      this.dispatchAction(history.location, location);
    }
  };

  handleLimit = () => {
    const limit = Math.floor(Math.random() * (80 - 0)) + 0;
    this.locationChange({ ...this.getParams(), limit });
  };

  handleOffset = () => {
    const offset = Math.floor(Math.random() * (80 - 0)) + 0;
    this.locationChange({ ...this.getParams(), offset });
  };

  handleActive = () => {
    this.locationChange({
      ...this.getParams(),
      active: !JSON.parse(this.getParams().active)
    });
  };

  render() {
    return (
      <div>
        <ul>
          {this.state.users.map(user => (
            <li key={user.id}>
              {user.name} <small>{user.email}</small>
            </li>
          ))}
        </ul>
        <button onClick={this.handleOffset}>Change offset</button>
        <button onClick={this.handleLimit}>Change limit</button>
        <button onClick={this.handleActive}>Change active</button>
        <br />
        <code>{JSON.stringify(this.getParams())}</code>
      </div>
    );
  }
}

export default Users;
